-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2016 at 08:58 AM
-- Server version: 5.5.25a
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `acg_local`
--

-- --------------------------------------------------------

--
-- Table structure for table `activasi`
--

CREATE TABLE IF NOT EXISTS `activasi` (
`id` int(20) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `registrasi` varchar(255) NOT NULL,
  `activasi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activator_codes`
--

CREATE TABLE IF NOT EXISTS `activator_codes` (
`id_pengguna` int(25) NOT NULL,
  `serial_nomor` varchar(50) NOT NULL,
  `registrasi_nomor` varchar(50) NOT NULL,
  `aktivasi_nomor` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=212 ;

--
-- Dumping data for table `activator_codes`
--

INSERT INTO `activator_codes` (`id_pengguna`, `serial_nomor`, `registrasi_nomor`, `aktivasi_nomor`) VALUES
(1, '100414169761', '5BF20D693396DCAC', 'EEAC0EA72E222C88ADCB'),
(2, '110715667223', '44670ED08DD0F954', '47D354E67D4659818FA4'),
(3, '100615458575', '17C5774BF05ADFBB', '1F2C67D0CB13BB191E5D'),
(4, '130416415227', '04B6988F50BB6A6F', '92BE45E6AEF3256BB95B'),
(5, '100513750578', 'DDA9D9FC140992A5', '6BACAA93F1BDF989362D'),
(6, '100614461844', 'DB82A157232F32EB', '6ED53EF7D075A30B4D88'),
(7, '110816710476', 'D90F49427B3FAB82', 'D134ED09F73690BD82A3'),
(8, '110911394234', 'C50AE6A8140DEC29', '317A59C43DDF01834745'),
(9, '110712878997', 'D9F45C6024A33A2B', 'A886E99C90ADF248430F'),
(10, '110811196895', '0011B1AD11B5FC4E', '2168B7472C1E8626CFEF'),
(11, '100419074958', '38387515A06CAC8C', '6A0A284F6A631C233051'),
(12, '100514109584', 'E5FFAC0137BDFD59', 'B0009D879143B9BDA481'),
(13, '100612838894', 'AF5005DB3BF9B0E2', 'B1CC7ECEF4A80ECC526A'),
(14, '110916326918', 'D24552D982165C8B', 'A14EE4B2F7C4506BDFCA'),
(15, '140916014891', 'AFD8C51B793B3DD4', '0F2188CC8DD30BC6B193'),
(16, '140815889849', 'BFDAF92E0244FCA7', '5A24175278E1BB0E28B8'),
(17, '140711473946', '4D3B944450DC62D2', 'E3B1ACBA366108D2DB6E'),
(18, '130410599554', '19F9C1838206E9A0', '789C8BC72948CE87E1C4'),
(19, '130519556653', 'D6041FD2A31674DC', '9212515ED3603BB81012'),
(20, '130611152062', '2C161931D0254E4A', '21F39267DB4A11281B25'),
(21, '140711725063', '671757F3564A4705', 'DFD23954DFD5B02943E7'),
(22, '120715413569', '5538157147D33CD6', '4664E8020B8AEA77F4A5'),
(23, '120811272463', 'C4AF997E566D084F', '68E2B8A8D5CBB4172ACB'),
(24, '140717689628', '7A37A9F4379371B6', '7391E1F004A394124165'),
(25, '110715922925', '79E9BAA0DEA76713', 'B8D6851FCE2EA7B84C84'),
(26, '110812230495', 'EAF55E7B9D5F9DAA', 'C80A2343140860FF4A12'),
(27, '110914495152', 'B7E57AA00C3FB8E4', 'E275A1A3A576694BDD2D'),
(28, '140415509771', 'B04E98B5B51D3CE9', '1D1AD1F0BC216C2ECD6A'),
(29, '140518083823', 'C9417447B507029B', '06B1E99AA2A14EC58AFC'),
(30, '140610053104', '1B57BCCF28310B0E', 'B331EAA8DB101A373F7F'),
(31, '140815790708', 'F9956A9C804742DC', '891544F976B0B0FB1B47'),
(32, '140910451271', '6B538F947B3336BE', 'BAFA285D4F6DE9420CCF'),
(33, '140819955414', 'E9662DA732228BC6', '330B4B827B40841DEAB6'),
(34, '130818055357', '5849E182A6C1DC5F', '8A6A249BC12AB2842E08'),
(35, '120715555732', '8A836785ACDDF047', 'F81C7D8A264F1D91C084'),
(36, '110715707631', '7BC68D871C155B22', '009185D2E2BBF4B56B15'),
(37, '110817484311', '3B60A151CBE62B3C', 'CC067519579D7D77E6B8'),
(38, '110910647046', '2B5E751E156A31D4', '69A6FADA9973257C7BFA'),
(39, '130710976189', '03F9F9830C828241', 'D3676F699393CE115057'),
(40, '130814765777', 'B3365776EBA0AB00', '2DCB03FD0E502250CB9C'),
(41, '130910203053', '6B9428B9CEC7C827', '6DA59C24263A6FF87E76'),
(42, '120913912875', '9D08978320EC25B3', '4CFCB69B0CCAC536A2FB'),
(43, '110712136909', 'B941C84403ACBA51', '2F7A9C646A24FE2F4074'),
(44, '110915863879', '1BBF23C003CA1BA9', '06EEBBAC32D4C58D73EB'),
(45, '110816737933', '1004F46540C4DD2D', 'BF46323BA6E2DDB94356'),
(48, '110712651998', 'AB794E2778BD48DC', 'B3E7289C82A4FF654290'),
(49, '110819371565', '8DD2A4ED0CAE03F8', 'C2ABBB571DA5536AF416'),
(50, '110912006902', '67721D32395689BD', '717AB656C4342D350FA7'),
(51, '110811645743', 'D308841ED0702B19', 'B8F54D7BEB6FD74D41F9'),
(52, '110819107498', '91A43F5A0AF2AAC8', '80C46FA734864B50098A'),
(53, '110915661232', '244D51166BBD0878', 'ADBC2B8787BE753CFBBB'),
(54, '130417255242', '92B60E9F5C3051D3', '53C8DAFD0B07D03F01ED'),
(55, '130517336694', '24396DE52C452300', '428DB572A275C3CBD3A2'),
(56, '130615115917', 'F1DA84C5D930A1C4', '9333D31EAD5CAAEE85AC'),
(57, '130717855642', 'F43F01720BE3460A', 'F16BD0E9C39C76D4D994'),
(58, '130817483253', '9F7C8DC3FF0BBB7D', '8B8CC38CE93EA3F2DEB9'),
(59, '130917287935', '2EDF5EC50C770FB3', 'E5D3DAFCFCBE7E0AC8CA'),
(60, '140417930223', '1BFC7813F5DBD489', 'B8054C8A4FE321336BC0'),
(61, '140517946145', '03E114D5E158DCE7', 'DEF28CA567D9F0EBFB17'),
(62, '140617559111', 'DA454618ABDCBA44', '51211F1E759C5A46E08E'),
(63, '140717098762', '4CCB33F2BA85B585', 'CB829359E8D2968C316B'),
(64, '140817198512', 'ED4CB54C69A5D8D9', 'B0B3E08C0A3147C964D6'),
(65, '140917919502', 'BF13560FE57D9D68', 'B3E05135F8616AC0FD47'),
(66, '120717645465', 'FAD4752620D7021A', 'DB0575B6BC00A385EBF4'),
(67, '120817729797', '4368254E22C011DA', 'B448E4452F17A95569C3'),
(68, '120917641736', 'A52BE6F21F781325', '75721DCFD3DDCEA8851B'),
(69, '110717847641', '8CC4E8F367EE90AC', '30B1E4005BD697C1DC4E'),
(70, '110817469262', '30D333589112C167', 'B10FB6E829C847991805'),
(71, '110917461623', '29254D69C9AC7E6C', 'BF59FA138D98ADA204F7'),
(72, '100417125661', '2967A148B69E0BE1', 'DF5B517F9DA4EEA2AD83'),
(73, '100517761266', '602A9656625BCC94', '1E526DDA1C570E4A935D'),
(74, '100617572134', 'B16ED03F957B670B', 'CEB8A3DA64F2CAA47BB8'),
(75, '130417982654', 'A80BC3E1E76C4517', 'D74416B1456195D47979'),
(76, '130517198615', 'F926C06AE0732038', 'F51BE0035E016C8AAC93'),
(77, '130619504728', 'D5DD9E860F83E397', 'A5C2E31F79FAC1E97819'),
(78, '130717002948', '28EC5A679D900E15', 'D8D90409D056182F288D'),
(79, '130817423283', '72F5381DB4722CDC', '0CE3AA28B9C46F28475A'),
(80, '130917871928', '8631BEA94B7AAFD4', 'EF1D7926EA8E0FF1728D'),
(81, '140417252743', 'CF31A33532265BAF', 'F99DFEA8F3D547B8BF28'),
(82, '140517428722', 'D96A590838CE0DA1', 'A4D99346043FD843FB19'),
(83, '140617894286', '140617894286', '6CF55B883F80FC4BFB62'),
(84, '140717968196', 'E00561DA07169F81', '8E41FBFFA0E79C85389A'),
(85, '140817557204', 'D6212F456DB2DB65', '5818A96D2833F54D6CBE'),
(86, '140917800751', '4880ECBCDFD6672A', '6F37CD1266448CF70315'),
(87, '120717817809', 'B38F4A8EB32B1BEC', '0805534095AC48253E10'),
(88, '120817415181', '4C7A12749088AB0C', 'D7F7A9FE7AF1190C2023'),
(89, '120917457646', '11581982A39CB726', '9346DC624A8D7F381258'),
(90, '110717382938', '445F0CC2A45DF688', '79C515C71CCCA097C64D'),
(91, '110817501544', '0071A4C7B02EC21E', '54487472181933F50659'),
(92, '110917297662', 'C8B9E5EA29D56B12', 'E99959EE08EAE7898E80'),
(93, '100417446117', 'FB2BC8EDC0E66D5B', '0D322BAD8FC269910630'),
(94, '100517021935', '14175F1BB951BF5A', '8AD7DC2CAC328FC25394'),
(95, '100617518061', '8FE142BFC04AC4C1', '435A3F64C05024E96219'),
(96, '120813689805', 'E5D51BDA27361B6E', 'A4FE5EA12B5E6A645140'),
(97, '120717449207', '3E55790EC327ACFE', '93BECEEDF417923199BA'),
(98, '120911814297', 'D0812DCFA12D783B', '07ADE63DA18CF6DA494E'),
(99, '110715144041', 'E1BDE07E3F7EEF36', '836CDCE9206F31DEFC96'),
(100, '110819804078', '1F18C9B7F5CC60F6', 'F0921F8133FEF1F24B1E'),
(101, '110917024199', 'F13E6050AA8D520C', '064B742EC5C248FC2619'),
(104, '151112572251', 'FF07F72836778788', '3BF9B3CE283F0BEED7C9'),
(105, '151213053961', '294EAAAD73535E69', '75007BB1B6B8702FB664'),
(106, '120819021433', 'BC8D5490B4CECA10', '971383A69B9AEB8462B5'),
(107, '120713081707', '646742BA87914D25', 'FA84319ACBF376C9E737'),
(108, '120917942738', 'E542CC55F7141B14', '8BDA4F8DCACB65B99627'),
(110, '140815210699', '676275B2EC604909', '4800B9FA23B9A24B56D6'),
(111, '140716407626', '919BCADDC04E848B', '0F54C543E08C4FB8ED41'),
(112, '151010135201', 'D274AB3F89CB8798', '75199C0930C712588704'),
(113, '151119304211', '0EFA1AEA8496B1D9', 'A36975468D65FF18AC7E'),
(114, '151211422184', 'FA59E3345B1F84E8', 'D0F9FF83A80B7CEAF066'),
(115, '151012879913', '05551229C8974EE0', '491910742A76C125E53C'),
(116, '120714227358', 'CE53F46AD1878E5C', 'E863421DE7ABFE0CE5B9'),
(117, '120919750162', '34619C7BCB24F04E', '08E5B9E53406CF484142'),
(118, '120810573234', '3EAE7DAC3A98FBB9', 'AEA64D99DB08315CC9F6'),
(119, '110718536789', 'BDE6997D40398E74', '211F275706A55F8E18EF'),
(120, '110813047427', '4F05DF1B734CD365', '84AC74FAF5DAA211AFCF'),
(121, '110914227332', '0BC9B59545C9849E', '709913E319E5217C7021'),
(122, '130417687873', 'EDDE5CA6DB0120B7', '2D634DCC7F44879014F7'),
(123, '100418481626', 'F41B11C7FDFF8E77', '379F2DB3E4EAF5BA5A17'),
(124, '100616818835', 'DF6660A9BC43899D', '822BCEE8919709470ED6'),
(125, '130618452523', '85F9246829929B60', 'C3B0B9197A1E06AC3E91'),
(126, '130516674194', 'FA72AA455480B400', 'BC5C98A31584C757423C'),
(127, '140418544841', 'E53BBF847AF7AB7E', 'F28DB2DB9D22506AD74D'),
(128, '140513255046', '838463F411462D1F', '600C990668547AD69F8F'),
(129, '140617843762', '20ACE745F11195BF', '08264CF0B96FA7AC594A'),
(130, '100417329867', '136D908439B22F2A', '5A6B0A0320A466B4CEFC'),
(131, '100517194286', 'DED5BADB77F2F2FE', 'C59491674444ABC9AB08'),
(132, '100617339351', '32FFB9A91DF03DAB', '0B8ED49B58B866453346'),
(133, '100615395918', '2E3D04558FAE4C63', '0CF289160238E55BB0B7'),
(134, '100618671034', '854E7D48E70A5496', '69F3639F849D84E12A85'),
(135, '100612618627', 'B14D01F4249842B0', 'D0861DA2BFEEBA9F9760'),
(136, '100618273302', 'E4F7C34D22049B3F', '0F09CA956D4E668F3C65'),
(137, '100410390619', '690A944A82FC3FA4', '97714042C30D4A5E89DA'),
(138, '100614507372', '7FF1C83A768F6375', 'F0236054951EAD461CE1'),
(139, '100612363679', 'AF1362D0794286C8', '0D2AE076808FF8BBE204'),
(140, '100616607774', 'FFA912E60D4531B6', '7DF5ADC4BA61F351B21F'),
(141, '100510372400', 'D30F3FFCCDCF12DD', '6239B734819A89A5CD89'),
(142, '100618366544', 'F702DC78BC39B131', 'BA0ABAE0A8155728119A'),
(143, '100618236796', 'B60E268AA1C6D863', 'E1D14031F7D8F8633271'),
(144, '100519826422', 'BCA097BC1169A119', 'DD3A2535771C442903CF'),
(145, '100510782939', 'C65C7177C7A60E30', '430832A36AA4AF567BE4'),
(146, '100510888678', '891289B5AC5990C2', '8818B2157FF33539FC90'),
(147, '100417833181', '812C18ACFA623F38', '05EE1363134394B99E95'),
(148, '100512286178', 'D3D4DAFFE2691335', '3F5F210EF1640EB65E74'),
(149, '100418056303', '1B3AF90F1416832B', '2351DF45540C3A4831CB'),
(150, '100612010155', '4502ADD28CA9724D', '134451AFE45DE82B0CC2'),
(151, '100413199025', '239822C1E5CB06A6', '574FBC37A9F1F502F7BE'),
(152, '100518663842', 'D392D220BA7E7976', '301DD8C18E39164C6899'),
(153, '100615300892', '2BC1F54FD61FA242', '47B44C3815AD9F9F4BA9'),
(154, '151118609388', 'F391D3FAE252B89C', '44224AA084CCEBD9656E'),
(155, '151015508683', 'D565004CF672D850', '9635B8F5CC10F7A55D27'),
(156, '110812076542', '110812076542', 'A885EF51EB1CD2F1DE00'),
(157, '120936217377', 'AFD5A2354B8233EC', '2AE1A62C12A94D44AD46'),
(159, '130636161098', '044A9762DF35AA3B', '84739F79BEEBB7BC5FE4'),
(160, '130613090815', '130613090815', '9E70F83435167CA6BBB4'),
(161, '130431232755', '2152DC8DF24513E6', 'A04AB36CC01B51F537C9'),
(162, '100431234093', '65D2563B150607D7', '1E3B4095CF51B1E40D8A'),
(163, '140431237811', '028ECC11CFBC64D2', '5E7ED7AD813326543A44'),
(164, '130436564434', '85FC3D8E5D37C727', '11C46CF69A865C64761E'),
(165, '140436594646', '02470FE06A3DC7FB', '5F76EAEF515BAFB72397'),
(166, '100436477242', '09D65EC015B07430', 'AF00263AFFD76342F4F9'),
(167, '130431231302', '83393CAE0D728BA6', '2A22C1CD3C4695FCFD78'),
(168, '100431239514', 'C4BB3198093C77AA', '88DD78F7E49E26700742'),
(169, '140431238017', '8C1492C9566891EB', '990E7BA8539BFD4746CE'),
(170, '130431232094', 'D1FE7851C119F543', '6210EC27F4D9A6CC18EA'),
(171, '100431236775', '3AC3B2D89618D73E', 'DF78E1BE92D01FA0347D'),
(172, '140431239825', '4770947885010F74', '142D08738860E318CC75'),
(173, '130831565523', '028AB983BADFC7CE', 'FAE7C640B76E00CAD0C8'),
(174, '151217425223', 'D5FAEBFC95C7A0FA', '85084BA219F9727FCD32'),
(175, '151219216869', '707E78C6F055C796', '4ABF94E0B24C11011F36'),
(176, '140919414734', '4F5C5C8E39622936', '9A2201ABEF6803D1F67C'),
(177, '140712061138', '4C3729E9B1A899D8', 'FCE2304576CC9E83201E'),
(178, '140815712686', 'B923AC1F65F0032A', 'D9612FF1EEE862E4E81E'),
(179, '130431238695', 'A01781A5F359EF38', 'E89835DEABDB781D005C'),
(180, '100431233426', 'BAD37C06722BF382', '7FC199531CDABEBEF42B'),
(181, '140431236805', 'B6138060BBA90540', 'E7A9DC03952CC01387C7'),
(182, '100417592563', '3009E35F0A073BAB', 'AECC8942DE509DBC2256'),
(183, '100617084056', 'CBF0D91C45E91706', '852B213313857059D013'),
(184, '130417922916', '2C6400D6DF2822A2', '2A2604FD82C6F7ACFFCE'),
(185, '130517462888', '7A5960AAD340AAEE', '7256F8192B6A9188AABD'),
(186, '130616820416', 'D9F87CC274622ECC', 'D799F7790E77A62C3BE7'),
(187, '100417570775', '33E118928953D101', '1D16C712B329DA2E2882'),
(188, '44065055095', 'a9a1e40eddf527be937615f444690322', 'E2328BCA998F1B55FB47'),
(189, '130431230312', '09E9945D3592D70F', '35BB33EEDAFF46A3B15A'),
(190, '100431230851', 'F0F7636969A6469C', 'B5B91728D9EFA595A153'),
(191, '140431236235', '209C731E4AA2016C', 'F33F91592996F0BBAE47'),
(192, '140531145716', 'F2A2D467E0796AD8', '15B251A76F1443A63810'),
(193, '130531151178', 'B3750F9B2476C2AE', '3F5DC409738538A30E28'),
(194, '140717644276', 'FFD8F3A471B4CB12', '3114EF30B815E77EB613'),
(195, '130713675804', '8E887E10437D49B5', '6F6B93314CB7A1247AC6'),
(196, '140812592313', '3EDBEEA57DED59DD', '060459C842C59E6D297A'),
(197, '140915225456', '642CD104D4FA14A2', 'C9C61441177667499D20'),
(198, '130810374236', 'D3C4CC7F8A01AA08', '40D8E733A2EDDD6CAD80'),
(199, '130912653412', 'CA103CB56FFC6562', '5D42EB7971201423BB5E'),
(200, '120719627677', '1A486813581FEF75', 'F1CF9E1B53C303212BA6'),
(201, '120810744868', 'DF836820CCAEDF0A', 'DA2091CD832F7C0DD5A6'),
(202, '120913946741', '1B53069B7785C3D1', '18624F77EDDA9CED697E'),
(203, '110718011742', 'A1D7B87E8CC1218B', 'F3B309A52B0539934E77'),
(204, '110714023501', 'A1D7B87E8CC1218B', '05D2E1DAC123F0ABDBB3'),
(205, '110718656587', 'A1D7B87E8CC1218B', '2C01ADD94382CDA8B5A6'),
(206, '1107169962737', 'A1D7B87E8CC1218B', 'E445A2989612ABA5413F'),
(207, '110910441507', 'CD50E568CB030462', '17B5CC81FB02FCC30CD6'),
(208, '110714076855', 'A1D7B87E8CC1218B', 'B2A770959950BEBDFB5B'),
(209, '110813940472', '458A4E1B8702C662', 'A4C5B1E15735EF962AA5'),
(210, '1109100200086', 'CD50E568CB030462', 'F7BE092CEA915344D17E'),
(211, '1107117854002', '62B7211F81FED2B', 'C97D7F0FDF89AA900875');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `username`, `password`) VALUES
(1, 'admin', 'e448bd81b32d1f1e052395b64d58a0e3');

-- --------------------------------------------------------

--
-- Table structure for table `batas_pengguna`
--

CREATE TABLE IF NOT EXISTS `batas_pengguna` (
  `kode_pengguna` varchar(10) NOT NULL,
  `kode_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batas_pengguna`
--

INSERT INTO `batas_pengguna` (`kode_pengguna`, `kode_menu`) VALUES
('admin', 1),
('admin', 2),
('admin', 3),
('admin', 4),
('admin', 5),
('admin', 6),
('admin', 7),
('admin', 8),
('admin', 9),
('admin', 10),
('admin', 99),
('support', 1),
('support', 2),
('support', 3),
('support', 4),
('support', 5),
('support', 9),
('support', 10),
('support', 99),
('user', 1),
('user', 2),
('user', 3),
('user', 4),
('user', 5),
('user', 9),
('user', 10),
('user', 99);

-- --------------------------------------------------------

--
-- Table structure for table `os`
--

CREATE TABLE IF NOT EXISTS `os` (
  `jenis_os` int(25) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `os`
--

INSERT INTO `os` (`jenis_os`, `nama`) VALUES
(1, 'WINDOWS'),
(2, 'LINUX');

-- --------------------------------------------------------

--
-- Table structure for table `serial`
--

CREATE TABLE IF NOT EXISTS `serial` (
`no` int(100) NOT NULL,
  `serial_nomor` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=219 ;

--
-- Dumping data for table `serial`
--

INSERT INTO `serial` (`no`, `serial_nomor`) VALUES
(1, '100414169761'),
(2, '110715667223'),
(3, '100615458575'),
(4, '130416415227'),
(5, '100513750578'),
(6, '100614461844'),
(7, '110816710476'),
(8, '110911394234'),
(9, '110712878997'),
(10, '110811196895'),
(11, '100419074958'),
(12, '100514109584'),
(13, '100612838894'),
(14, '110916326918'),
(15, '140916014891'),
(16, '140815889849'),
(17, '140711473946'),
(18, '130410599554'),
(19, '130519556653'),
(20, '130611152062'),
(21, '140711725063'),
(22, '120715413569'),
(23, '120811272463'),
(24, '140717689628'),
(25, '110715922925'),
(26, '110812230495'),
(27, '110914495152'),
(28, '140415509771'),
(29, '140518083823'),
(30, '140610053104'),
(31, '140815790708'),
(32, '140910451271'),
(33, '140819955414'),
(34, '130818055357'),
(35, '130436003755'),
(36, '120717645465'),
(37, '120817729797'),
(38, '120917641736'),
(39, '120715555732'),
(40, '110715707631'),
(41, '110817484311'),
(42, '110910647046'),
(43, '130710976189'),
(44, '130814765777'),
(45, '130910203053'),
(46, '120913912875'),
(47, '110712136909'),
(48, '110915863879'),
(49, '110816737933'),
(50, '110725676529'),
(51, '110728972792'),
(52, '110712651998'),
(53, '110819371565'),
(54, '110912006902'),
(55, '110811645743'),
(56, '110819107498'),
(57, '100411521329'),
(58, '100517545412'),
(59, '100617369564'),
(60, '110915661232'),
(61, '130417255242'),
(62, '130517336694'),
(63, '130615115917'),
(64, '130717855642'),
(65, '130817483253'),
(66, '130917287935'),
(67, '140417930223'),
(68, '140517946145'),
(69, '140617559111'),
(70, '140717098762'),
(71, '140817198512'),
(72, '140917919502'),
(73, '120717645465'),
(74, '120817729797'),
(75, '120917641736'),
(76, '110717847641'),
(77, '110817469262'),
(78, '110917461623'),
(79, '100417125661'),
(80, '100517761266'),
(81, '100617572134'),
(82, '130417982654'),
(83, '130517198615'),
(84, '130619504728'),
(85, '130717002948'),
(86, '130817423283'),
(87, '130917871928'),
(88, '140417252743'),
(89, '140517428722'),
(90, '140617894286'),
(91, '140717968196'),
(92, '140817557204'),
(93, '140917800751'),
(94, '120717817809'),
(95, '120817415181'),
(96, '120917457646'),
(97, '110717382938'),
(98, '110817501544'),
(99, '110917297662'),
(100, '100417446117'),
(101, '100517021935'),
(102, '100617518061'),
(103, '120813689805'),
(104, '120717449207'),
(105, '120911814297'),
(106, '110715144041'),
(107, '110819804078'),
(108, '110917024199'),
(109, '151014758446'),
(110, '151014758446'),
(111, '151112572251'),
(112, '151213053961'),
(113, '120819021433'),
(114, '120713081707'),
(115, '120917942738'),
(116, '15101012217'),
(117, '140815210699'),
(118, '140716407626'),
(119, '151010135201'),
(120, '151119304211'),
(121, '151211422184'),
(122, '151012879913'),
(123, '120714227358'),
(124, '120919750162'),
(125, '120810573234'),
(126, '110718536789'),
(127, '110813047427'),
(128, '110914227332'),
(129, '130417687873'),
(130, '100418481626'),
(131, '100616818835'),
(132, '130618452523'),
(133, '130516674194'),
(134, '140418544841'),
(135, '140513255046'),
(136, '140617843762'),
(137, '100417329867'),
(138, '100517194286'),
(139, '100617339351'),
(140, '100615395918'),
(141, '100618671034'),
(142, '100612618627'),
(143, '100618273302'),
(144, '100410390619'),
(145, '100614507372'),
(146, '100612363679'),
(147, '100616607774'),
(148, '100510372400'),
(149, '100618366544'),
(150, '100618236796'),
(151, '100519826422'),
(152, '100510782939'),
(153, '100510888678'),
(154, '100417833181'),
(155, '100512286178'),
(156, '100418056303'),
(157, '100612010155'),
(158, '100413199025'),
(159, '100518663842'),
(160, '100615300892'),
(161, '151118609388'),
(162, '151015508683'),
(163, '110812076542'),
(164, '120936217377'),
(165, '130636161098'),
(166, '130636161098'),
(167, '130613090815'),
(168, '130431232755'),
(169, '100431234093'),
(170, '140431237811'),
(171, '130436564434'),
(172, '140436594646'),
(173, '100436477242'),
(174, '130431231302'),
(175, '100431239514'),
(176, '140431238017'),
(177, '130431232094'),
(178, '100431236775'),
(179, '140431239825'),
(180, '130831565523'),
(181, '151217425223'),
(182, '151219216869'),
(183, '140919414734'),
(184, '140712061138'),
(185, '140815712686'),
(186, '130431238695'),
(187, '100431233426'),
(188, '140431236805'),
(189, '100417592563'),
(190, '100617084056'),
(191, '130417922916'),
(192, '130517462888'),
(193, '130616820416'),
(194, '100417570775'),
(195, '44065055095'),
(196, '130431230312'),
(197, '100431230851'),
(198, '140431236235'),
(199, '140531145716'),
(200, '130531151178'),
(201, '140717644276'),
(202, '130713675804'),
(203, '140812592313'),
(204, '140915225456'),
(205, '130810374236'),
(206, '130912653412'),
(207, '120719627677'),
(208, '120810744868'),
(209, '120913946741'),
(210, '110718011742'),
(211, '110714023501'),
(212, '110718656587'),
(213, '1107169962737'),
(214, '110910441507'),
(215, '110714076855'),
(216, '110813940472'),
(217, '1109100200086'),
(218, '1107117854002');

-- --------------------------------------------------------

--
-- Table structure for table `serial_number`
--

CREATE TABLE IF NOT EXISTS `serial_number` (
`id` int(50) NOT NULL,
  `serial_no` varchar(250) NOT NULL,
  `subjects` varchar(255) NOT NULL,
  `type_application` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `serial_number`
--

INSERT INTO `serial_number` (`id`, `serial_no`, `subjects`, `type_application`, `date`) VALUES
(2, '140714565839', 'pengupdatean bhs inggris', 'bahasa inggris', '2014-01-22 06:20:54'),
(3, '120716213018', 'pengupdatean bhs', '  indonesia bhasa', '2014-01-22 06:21:02'),
(6, '120719579787', 'bahasa indonesia', 'bahan pembelajaran bahasa indonesia dan bahasa jepang', '2014-01-22 06:21:11'),
(7, '120717242032', 'penguploadan', 'bhs.indonesia', '2014-01-22 06:21:20'),
(8, '140910902422', 'penguploadan', 'bhs.indonesia', '2014-01-22 06:21:28'),
(9, '110713866876', 'fsag', 'dgsgs', '2014-01-22 06:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(20) NOT NULL,
  `kode_pengguna` varchar(10) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `kode_pengguna`, `nama`, `level`, `password`) VALUES
(1, 'admin', 'admin', 1, 'admin'),
(2, 'support', 'support', 1, 'support'),
(3, 'users', 'nindia', 3, 'najiha'),
(4, 'users', 'hadi', 2, 'mamur hadiana'),
(7, 'users', 'martinus', 1, 'najiha');

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE IF NOT EXISTS `user_data` (
`id_pengguna` int(11) NOT NULL,
  `nama_pengguna` varchar(100) NOT NULL,
  `jenis_aplikasi` varchar(100) NOT NULL,
  `nama_sekolah` varchar(100) NOT NULL,
  `alamat_sekolah` varchar(250) NOT NULL,
  `nomor_telepon` varchar(100) NOT NULL,
  `email` varchar(250) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=212 ;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`id_pengguna`, `nama_pengguna`, `jenis_aplikasi`, `nama_sekolah`, `alamat_sekolah`, `nomor_telepon`, `email`) VALUES
(1, 'pp04pt', 'IPS Kelas 4', 'pp04pt', 'Jln Raya Pondok Pinang no 3', '081314501351', 'supatmipatmi92@yahoo.com'),
(2, 'ruaida', 'Geografi Kelas 7', 'ruaida', 'Jl. KH. Wahid Hasyim, Gg. H. Lamin No. 35', '081384457186', 'yarham.s89@gmail.com'),
(3, 'SDN CIPEDAK 03', 'IPS Kelas 6', 'SDN CIPEDAK 03', 'jalan timbul', '021-7872835', 'mansmanan@gmail.com'),
(4, 'Poniman', 'B. Indonesia Kelas 4', 'Poniman', 'Jalan Angke Indah gang VII', '6308054', 'poniman14@yahoo.co.id'),
(5, 'Supatmi', 'IPS Kelas 5', 'Supatmi', 'jl. raya pondok pinang no.3', '081314501351', 'supatmipatmi92@yahoo.com'),
(6, 'Supatmi', 'IPS Kelas 6', 'Supatmi', 'jl. raya pondok pinang no.3', '081314501351', 'supatmipatmi92@yahoo.com'),
(7, 'ruaida', 'Geografi Kelas 8', 'ruaida', ' Jl. KH. Wahid Hasyim, Gg. H. Lamin No. 35', '081384457186', 'ruaidaf@yahoo.com'),
(8, 'ruaida', 'Geografi Kelas 9', 'ruaida', 'JL. KH. Wahid Hasyim, Gg. H. Lamin No. 35', '081384457186', 'ruaidaf@yahoo.co.id'),
(9, 'SMPN 19 Jakarta', 'Geografi Kelas 7', 'SMPN 19 Jakarta', 'Jl.Bumi Blok E No.21', '7250219', 'smp19jkt@gmail.com'),
(10, 'SMPN 19 JKT', 'Geografi Kelas 8', 'SMPN 19 JKT', 'Jl. Bumi Blok E No.21', '021-7250219', 'smp19jkt@gmail.com'),
(11, 'BAPAK BUANG ', 'IPS Kelas 4', 'BAPAK BUANG ', 'Jl. Gandaria Tengah 2', '02126225100', 'devit_kpr@yahoo.co.id'),
(12, 'BAPAK BUANG ', 'IPS Kelas 5', 'BAPAK BUANG ', 'Jl. Bumi ', '02126225100', 'devit_kpr@yahoo.co.id'),
(13, 'BAPAK BUANG ', 'IPS Kelas 6', 'BAPAK BUANG ', 'Jl. Bumi ', '02126225100', 'devit_kpr@yahoo.co.id'),
(14, ' SMPN 19 JAKARTA', 'Geografi Kelas 9', ' SMPN 19 JAKARTA', 'Jl.Bumi Blok E No.21', '021-7250219', 'smp19jkt@gmail.com'),
(15, 'bustanul fahrudin', 'PKn Kelas 9', 'bustanul fahrudin', 'pup blok ak20 no.15,bekasi', '081314094173', 'bustanul_fahrudin@yahoo.co.id'),
(16, 'bustanul fahrudin', 'PKn Kelas 8', 'bustanul fahrudin', 'pup blok ak20 no.15,bekasi', '081314094173', 'bustanul_fahrudin@yahoo.co.id'),
(17, 'bustanul fahrudin', 'PKn Kelas 7', 'bustanul fahrudin', 'pup blok ak20 no.15,bekasi', '081314094173', 'bustanul_fahrudin@yahoo.co.id'),
(18, 'Poniman', 'B.Indonesia 4', 'Poniman', 'jalan mansyur', '081380441578', 'poniman14@yahoo.co.id'),
(19, 'Poniman', 'B.Indonesia 5', 'Poniman', 'jalan mansyur', '081380441578', 'poniman14@yahoo.co.id'),
(20, 'Poniman', 'B.Indonesia 6', 'Poniman', 'jalan mansyur', '081380441578', 'poniman14@yahoo.co.id'),
(21, 'Rohati', 'Pkn Kelas 7', 'Rohati', 'Villa Permata Santi Blak A no.9', '085711300229', 'rohatiarami@gmail.com'),
(22, 'Daluri', 'Ekonomi Kelas 7', 'SMP 226 Jakarta', 'SMP 226 Jakarta', '021 750 1270', 'denmasebagongdhaluri@yahoo.co.id'),
(23, 'Daluri', 'Ekonomi Kelas 8', 'SMP 226  Jakarta', 'SMP 226  Jakarta', '021 750 1270', 'denmasebagongdhaluri@yahoo.co.id'),
(24, 'Akhmadi', 'PKn Kelas 7', 'Akhmadi', 'Kebon jeruk', '081383795037', 'akhmadi_jpr@yahoo.co.id'),
(25, 'Guru IPS', 'Geografi Kelas 7', 'Guru IPS', 'Jl. Gandaria V Jagakarsa - Jaksel 12620', '0217270842', 'yosep.aw@gmail.com'),
(26, 'Guru IPS', 'Geografi Kelas 8', 'Guru IPS', 'Jl. Gandaria V Jagakarsa - Jaksel 12620', '0217270842', 'yosep.aw@gmail.com'),
(27, 'Guru IPS', 'Geografi Kelas 9', 'Guru IPS', 'Jl. Gandaria V Jagakarsa - Jaksel 12620', '0217270842', 'yosep.aw@gmail.com'),
(28, 'SDN GANDARIA UTARA 08', 'PKn Kelas 4', 'SDN GANDARIA UTARA 08', 'Jl.Hidup Baru 3 RT.11/07 No.56', '087821452316', 'fauzianina@gmail.com'),
(29, 'SDN GANDARIA UTARA 08', 'PKn Kelas 5', 'SDN GANDARIA UTARA 08', 'Jl.Hidup Baru 3 RT.11/07 No.56', '087821452316', 'fauzianina@gmail.com'),
(30, 'SDN GANDARIA UTARA 08', 'PKn Kelas 6', 'SDN GANDARIA UTARA 08', 'Jl.Hidup Baru 3 RT.11/07 No.56', '087821452316', 'fauzianina@gmail.com'),
(31, 'Akhmadi', 'PKn Kelas 8', 'Akhmadi', 'Jakarta Barat', '081383795037', 'akhmadi_jpr@yahoo.co.id'),
(32, 'akhmadi', 'PKn Kelas 9', 'akhmadi', 'jakarta barat', '081383795037', 'akhmadi_jpr@yahoo.co.id'),
(33, 'Akhmadi', 'PKn Kelas 8', 'Akhmadi', 'Jakarta Barat', '081383795037', 'akhmadi_jpr@yahoo.co.id'),
(34, 'bambang', 'B. Indonesia Kelas 8', 'bambang', 'bambang', '021 7192868', 'bambangutama@gmail.com'),
(35, 'Hadis', 'Ekonomi Kelas 7', 'Hadis', 'SMPN 87', '081383282234', 'hadis_latoe@yahoo.co.id'),
(36, 'Titik Purwanti', 'Geografi Kelas 7', 'SMPN 229 Jakarta', 'SMPN 229 Jakarta', '7501270', 'titikpurwanti@yahoo.co'),
(37, 'Titik Purwanti', 'Geografi Kelas 8', 'SMP N 225 Jakarta', 'SMP N 225 Jakarta', '7501270', 'titikpurwanti@yahoo.co'),
(38, 'Titik Purwanti', 'Geografi Kelas 9', 'SMP N 226 Jakarta', 'SMP N 226 Jakarta', '7501270', 'titikpurwanti@yahoo.co'),
(39, 'RETNO TANJUNG SARI', 'B. Indonesia Kelas 7', 'RETNO TANJUNG SARI', 'bukit pamulang indah F 3 no 2, tangsel', '08176343075', 'retnotanjungsari@gmail.com'),
(40, 'RETNO TANJUNG SARI', 'B. Indonesia Kelas 8', 'RETNO TANJUNG SARI', 'bukit pamulang indah F 3 no 2, tangsel', '08176343075', 'retnotanjungsari@gmail.com'),
(41, 'RETNO TANJUNG SARI', 'B. Indonesia Kelas 9', 'RETNO TANJUNG SARI', 'bukit pamulang indah F 3 no 2, tangsel', '08176343075', 'retnotanjungsari@gmail.com'),
(42, 'daluri', 'Ekonomi Kelas 9', 'SMP 226  Jakarta', 'SMP 226  Jakarta', '7501270', 'denmasebagongdhaluri@yahoo.co.id'),
(43, 'ARIFUDDIN', 'Geografi Kelas 7', 'SMPN 145 Jakarta', 'JL. Menteng Pulo ujung', '0218298205', 'smp145jakarta@yahoo.co.id'),
(44, 'ARIFUDDIN', 'Geografi Kelas 9', 'SMPN 145 Jakarta', 'jl. Menteng Pulo Ujung', '0218298205', 'smp145_jakarta@yahoo.co.id'),
(45, 'ARIFUDDIN', 'Geografi Kelas 8', 'SMPN 145 Jakarta', 'JL. Menteng Pulo ujung', '0218298205', 'smp145_jakarta@yahoo.co.id'),
(48, ' FARANI', 'Geografi Kelas 7', ' FARANI', 'JL.Panjang Cidodol', '0217394260', 'sa.liti@yahoo.co.id'),
(49, 'Farani', 'Geografi Kelas 8', 'Farani', 'JL.Panjang Cidodol', '0217394260', 'sa.liti@yahoo.co.id'),
(50, 'Farani', 'Geografi Kelas 9', 'Farani', 'JL.Panjang Cidodol', '0217394260', 'sa.liti@yahoo.co.id'),
(51, 'Titik Purwanti S.Pd', 'Geografi Kelas 8', 'SMP Negri 22', 'SMP Negri 22', '0217501270', 'titikpurwanti@yahoo.co.id'),
(52, 'euis maryati', 'Geografi Kelas 8', 'SMPN 144 Jakarta', 'SMPN 144 Jakarta', '0214617313', '144jkt@gmail.com'),
(53, 'Sudarmilah SPd', 'Geografi Kelas 9', 'smpn 163', 'smpn 163', '0818920200', 'darmilah90@yahoo.com'),
(54, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(55, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(56, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(57, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(58, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(59, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(60, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(61, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(62, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(63, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(64, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(65, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(66, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(67, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(68, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(69, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(70, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(71, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(72, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(73, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(74, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(75, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(76, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(77, 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad'),
(78, 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad', 'Di hibahkan oleh Pak Hariyad'),
(79, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(80, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(81, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(82, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(83, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(84, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(85, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(86, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(87, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(88, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(89, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(90, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(91, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(92, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(93, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(94, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(95, 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi', 'Di hibahkan oleh Pak Hariyadi'),
(96, 'Ramadhan Edwin', 'Ekonomi Kelas 8', 'SMPN 213 Jakarta', ' Jl. Malaka I Perumnas Klender, Jakarta Timur', '021', 'ramadhanedwin93@gmail.com'),
(97, 'Zulmalyar', 'Ekonomi 7', 'SMP 213 Jakarta Timur', 'Jl. Malaka I Perumnas Klender, Jakarta Timur', '02188854650', 'ramadhanedwin93@gmail.com'),
(98, 'Zulmalyar', 'Ekonomi Kelas 9', 'SMP 213 Jakarta Timur', 'Jl. Malaka I Perumnas Klender, Jakarta Timur', '02188854650', 'ramadhanedwin93@gmail.com'),
(99, 'Zulmalyar', 'Geografi Kelas 7', 'SMP 213 Jakarta Timur', 'Jl. Malaka I Perumnas Klender, Jakarta Timur', '02188854650', 'ramadhanedwin93@gmail.com'),
(100, 'Zulmalyar', 'Geografi Kelas 8', 'SMP 213 Jakarta Timur', 'Jl. Malaka I Perumnas Klender, Jakarta Timur', '02188854650', 'ramadhanedwin93@gmail.com'),
(101, 'Zulmalyar', 'Geografi Kelas 9', 'SMP 213 Jakarta Timur', 'Jl. Malaka I Perumnas Klender, Jakarta Timur', '02188854650', 'ramadhanedwin93@gmail.com'),
(104, 'Dra. Fauziah Ismail', 'Biolearn 11', 'SMAN 108 Jakarta', 'Jl.Kesadaran Ulujami Raya, Pesanggrahan, DKI Jakarta', '7376876', 'mak_fau@yahoo.co.id'),
(105, 'Dra. Fauziah Ismail', 'Biolearn 12', 'SMAN 108 Jakarta', 'Jl.Kesadaran Ulujami Raya, Pesanggrahan, DKI Jakarta', '7376876', 'mak_fau@yahoo.co.id'),
(106, 'SMPN 238 Jakarta', 'Ekonomi Kelas 7', 'SMPN 238', 'Jakarta', '0217991565', 'widji.wiwid@yahoo.co.id'),
(107, 'SMPN 238 Jakarta', 'NextEdu Ekonomi 7', 'SMPN 238 Jakarta', 'Jakarta', '0217991565', 'widji.wiwid@yahoo.co.id'),
(108, 'SMPN 238 Jakarta', 'NextEdu Ekonomi 9', 'SMPN 238 Jakarta', 'Jakarta', '0217991565', 'widji.wiwid@yahoo.co.id'),
(110, 'smpn205', 'PKn Kelas 8', 'smpn205', ' jl. semanan raya kalideres jakarta barat', '0215446287', 'yubagus205@gmail.com'),
(111, 'smpn205', 'PKn Kelas 7', 'smpn205', 'jl.semanan raya kalideres jakarta barat', '0215446287', 'smpn205jkt@gmail.com'),
(112, 'Kus Indriarti', 'Biolearn 10', 'SMA Negeri 79 Menteng Pulo', 'SMA Negeri 79 Menteng Pulo', '0218357087', 'kus_indriarti@ymail.com'),
(113, 'Kus Indriarti', 'Biolearn 11', 'SMA Negeri 79 Menteng Pulo', 'SMA Negeri 79 Menteng Pulo', '0218357087', 'kus_indriarti@ymail.com'),
(114, 'Kus Indriarti', 'Biolearn 12', 'SMA Negeri 79 Menteng Pulo', 'SMA Negeri 79 Menteng Pulo', '0215446287', 'kus_indriarti@ymail.com'),
(115, 'sman 85 jakarta', 'Biolearn 10', 'sman 85 jakarta', 'sman 85 jakarta', '021-5840921', 'yulfitriurangawak@yahoo.com'),
(116, 'smpn205', 'Ekonomi Kelas 7', 'smpn205', 'jl. semanan raya kalideres jakarta barat', '5446287', 'smpn205jkt@gmail.com'),
(117, 'smpn205jkt', 'Ekonomi Kelas 9', 'smpn205jkt', 'jl. semanan kalideres jakarta barat', '021 5446287', 'smpn205jkt@gmail.com'),
(118, 'smpn205', 'Ekonomi Kelas 8', 'smpn205', 'jl. semanan raya kalideres jakarta barat', '021 5446287', ' smpn205jkt@gmail.com'),
(119, 'smpn205', 'Geografi Kelas 7', 'smpn205', 'jl. semanan kalideres jakarta barat', '0215446287', 'smpn205jkt@gmail.com'),
(120, 'smpn205', 'Geografi Kelas 8', 'smpn205', 'jl.semanan raya kalideres jakarta barat', '0215446287', 'smpn205jkt@gmail.com'),
(121, 'smpn205', 'Geografi Kelas 9', 'smpn205', 'jl. semanan kalideres jakarta barat', '0215446287', 'smpn205jkt@gmail.com'),
(122, 'Nurjanatun', 'Bahasa Indonesia Kelas 4', ' SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen', '087783254078', 'nunky_nii@yahoo.co.id'),
(123, 'Nurjanatun', 'IPS Kelas 4', 'SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen, Jakarta Pusat', '087783254078 ', 'nunky_nii@yahoo.co.id'),
(124, 'Nurjanatun', 'IPS Kelas 6', 'SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen, Jakarta Pusat', '087879753999', 'nunky_nii@yahoo.co.id'),
(125, 'Nurjanatun', 'InteractiveLearning Bahasa Indonesia Kelas 6', 'SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen, Jakarta Pusat', '087879753999', 'nunky_nii@yahoo.co.id'),
(126, 'Nurjanatun', 'InteractiveLearning Bahasa Indonesia Kelas 5', 'SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen, Jakarta Pusat', '087879753999', 'nunky_nii@yahoo.co.id'),
(127, 'Nurjanatun', 'InteractiveLearning PKn Kelas 4', 'SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen, Jakarta Pusat', '087879753999', 'nunky_nii@yahoo.co.id'),
(128, 'Nurjanatun', 'InteractiveLearning PKn Kelas 5', 'SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen, Jakarta Pusat', '087879753999', 'nunky_nii@yahoo.co.id'),
(129, 'Nurjanatun', 'InteractiveLearning PKn Kelas 6', 'SDN Senen 03 Pagi', 'Jl. Abdur Rahman Saleh I / 8 Senen, Jakarta Pusat', '087879753999', 'nunky_nii@yahoo.co.id'),
(130, 'SMI', 'NextEdu IPS Garut Kelas 4', 'SMI', 'SMI', '021', 'ova.a@infisol.net'),
(131, 'SMI', 'NextEdu IPS Garut Kelas 4', 'SMI', 'SMI', '021', 'ova.a@infisol.net'),
(132, 'Coolmaster', 'IPS SD', 'SDN 05 Pagi', 'JKT', '021', 'd@y.com'),
(133, 'SDN Tanjungkamuning 1', 'IPS SD', 'SDN Tanjungkamuning 1', 'Kp. Tanjungkamuning', '', 'sdntanjungkamuning1@yahoo.co.id'),
(134, 'Bunga', 'IPS SD', 'SDN Kadungora 2', 'Jl. Panenjoan', '085295312781', 'bungakowex@yahoo.com'),
(135, 'SDN RANCASALAK 4', 'IPS SD', 'SDN RANCASALAK 4', 'KP. PENCLUT', '', 'sdnrancasalak4@gmail.com'),
(136, 'SDN Pamulihan 01', 'IPS SD Kelas 6', 'SDN Pamulihan 01', 'KP. Pangauban Ds. Pamulihan Cisurupan - Garut', '085320633248', 'juaspdsd@gmail.com'),
(137, 'SDN Tanjungkamuning 1', 'IPS SD', 'SDN Tanjungkamuning 1', 'Kp. Tanjungkamuning', '', 'sdntanjungkamuning1@yahoo.co.id'),
(138, 'infiniti', 'IPS SD kelas 6', 'SD infiniti', 'Garut', '022', 'garut@yahoo.com'),
(139, 'SDN SUDALARANG 3', 'IPS', 'SDN SUDALARANG 3', 'KP.CIJAMBE', '082320120908', 'sdnsudalarang333@gmail.com'),
(140, 'NOVAN ACHMAD RAHMANTULLOH', 'IPS', 'SDN SUKASONO 1', 'KP. NEGLASARI', '085222265477', 'novan.rahmantulloh@gmail.com'),
(141, 'SDN Tanjungkamuning 1', 'IPS SD', 'SDN Tanjungkamuning 1', 'Kp. Tanjungkamuning', '', 'sdntanjungkamuning1@yahoo.co.id'),
(142, 'sdn karamatwangi 03', 'ips sd kls 6', 'sdn karamatwangi 03', 'Kp.Janggol', '082316414031', 'a_aseps@yahoo.com'),
(143, 'SDN SUKATANI 1', 'IPS SD', 'SDN SUKATANI 1', 'KP. DESAKOLOT GARUT', '', 'sdnsukatanisatucilawu@gmail.com'),
(144, 'SDN SUDALARANG 3', 'IPS', 'SDN SUDALARANG 3', 'KP.CIJAMBE', '082320120908', 'sdnsudalarang333@gmail.com'),
(145, 'SDN RANCASALAK 4', 'IPS SD', 'SDN RANCASALAK 4', 'KP. PENCLUT', '', 'sdnrancasalak4@gmail.com'),
(146, 'NOVAN ACHMAD RAHMANTULLOH', 'IPS', 'SDN SUKASONO 1', 'KP. NEGLASARI', '085222265477', 'novan.rahmantulloh@gmail.com'),
(147, 'SDN SUKATANI 1', 'IPS SD', 'SDN SUKATANI 1', 'KP. DESAKOLOT GARUT', '', 'sdnsukatanisatucilawu@gmail.com'),
(148, 'Bunga', 'IPS SD', 'SDN Kadungora 2', 'Jl. Panenjoan', '085295312781', 'bungakowex@yahoo.com'),
(149, 'SDN RANCASALAK 4', 'IPS SD', 'SDN RANCASALAK 4', 'KP. PENCLUT', '', 'sdnrancasalak4@gmail.com'),
(150, 'SDN SUKAWARGI 02', 'IPS KELAS 6', 'SDN SUKAWARGI 02', 'KP. PANGGILINGAN', '087827252226', 'agussetianabahtiar@yahoo.co.id'),
(151, 'Bunga', 'IPS SD', 'SDN Kadungora 2', 'Jl. Panenjoan', '085295312781', 'bungakowex@yahoo.com'),
(152, 'SDN SUKATANI 1', 'IPS SD', 'SDN SUKATANI 1', 'KP. DESAKOLOT GARUT', '', 'sdnsukatanisatucilawu@gmail.com'),
(153, 'KOKOM', 'IPS SD', 'SDN SUKAGALIH 5', 'JL PATRIOT DALAM', '085223350069', 'kokomkomariah549@yahoo.co.id'),
(154, 'SMAN 34', 'Aplikasi Biolearn', 'SMAN 34', 'Jln. Margaswasta no 1 ', '087873999652', 'harysman34@gmail.com'),
(155, 'SMAN 34', 'Aplikasi Biolearn', 'SMAN 34', 'Jln.Margasatwa no 1', '087873999652', 'harysman34@gmail.com'),
(156, 'Ova Achmad Musofa', 'Aplikasi Biolearn', 'Infiniti Reka Solusi', 'Jakarta', '089693813123', 'mynameisova@gmail.com'),
(157, 'SARWONO', 'Aplikasi Yuk Belajar', 'Sarwo Media', 'Jl. Bungurasih Dalam no. 86 B, Waru, Sidoarjo', '081328532221', 'nuno_handal@yahoo.com'),
(159, 'SARWONO', 'Aplikasi Yuk Belajar', 'Sarwo Media', 'Jl. Bungurasih Dalam 86 B, Waru, Sidoarjo', '0813285332221', 'nuno_handal@yahoo.com'),
(160, 'Ova Achmad Musofa', 'Aolikasi NextEdu Compact', 'Test ACG', 'CityLoft Jakarta', '089693813123', 'mynameisova@gmail.com'),
(161, 'Indocamp', 'Aplikasi Yuk Belajar', 'Indocamp', 'Jl.Bangka raya no.2', '0217088875', 'indocam@yahoo.com'),
(162, 'Indocamp', 'Aplikasi Yuk Belajar', 'Indocamp', 'Jl.Bangka raya no.2', '0217088875', 'indocam@yahoo.com'),
(163, 'Indocamp', 'Aplikasi Yuk Belajar', 'Indocamp', 'Jl.Bangka Raya no 2', '0217088875', 'indocam@yahoo.com'),
(164, 'Khoirot', 'Aplikasi Yuk Belajar', 'SD AL MUSLIM', 'jl. Raya Wadungasri 39 F, Waru, Sidoarjo', '087855807176', 'sarwo.media@gmail.com'),
(165, 'Khoirot', 'Aplikasi Yuk Belajar', 'SD AL MUSLIM', 'Jl. Raya Wadungasri 39F, waru', '087855807176', 'sarwo.media@gmail.com'),
(166, 'Khoirot', 'Aplikasi Yuk Belajar', 'SD AL MUSLIM', 'Jl. Raya Wadungasri 39F, Waru, Sidoarjo', '087855807176', 'sarwo.media@gmail.com'),
(167, 'Indocamp', 'Aplikasi Yuk Belajar', 'Indocamp', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(168, 'Indocamp', 'Aplikasi Yuk Belajar', 'IndocampSmartSchool', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(169, 'Indocamp', 'Aplikasi Yuk Belajar', 'IndocampSmartSchool', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(170, 'Indocamp', 'Aplikasi Yuk Belajar', 'IndocampSmartSchool', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(171, 'Indocamp', 'Aplikasi Yuk Belajar', 'IndocampSmartSchool', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(172, 'Indocamp', 'Aplikasi Yuk Belajar', 'IndocampSmartSchool', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(173, 'Wahyu_Hilal', 'Aplikasi Yuk Belajar', 'SMP 231', 'Rorotan', '0816965577', 'wahyu_hilal@yahoo.co.id'),
(174, 'Andy', 'Aplikasi Biolearn', 'Infiniti', 'Jakarta', '021', 'myanmeisova@gmail.com'),
(175, 'SMAN 34', 'Aplikasi Biolearn', 'SMAN 34', 'Jln.Pondok Labu', '08787399652', 'harysman34@gmail.com'),
(176, 'Sri Purwanti', 'Aplikasi Interactive Learning', 'SMP 36 Jakarta Timur', 'Jl. Pedati', '082111213339', 'sripurwanti1206@gmail.com'),
(177, 'Sri purwanti', 'Aplikasi Interactive Learning', 'SMP 36 Jakarta Timur', 'Jl Pedati', '082111213339', 'sripurwanti1206@gmail.com'),
(178, 'Sri Purwanti', 'Aplikasi Interactive Learning', 'SMP 36 Jakarta Timur', 'Jl. Pedati Jakarta Timur', '082111213339', 'sripurwanti1206@gmail.com'),
(179, 'Indocamp', 'Aplikasi Yuk Belajar', 'Islamic Smart School', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(180, 'Indocamp', 'Aplikasi Yuk Belajar', 'Islamic Smart School', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(181, 'Indocamp', 'Aplikasi Yuk Belajar', 'Islamic Smart School', 'Jl. Bangka Raya No. 2', '02170888075', 'indocamp@yahoo.com'),
(182, 'Dendy', '0', 'Infiniti', 'Citilofts', '02125558785', 'dendyrezki@yahoo.com'),
(183, 'Dendy', 'Aplikasi NextEdu', 'Infiniti', 'Citilofts', '02125558785', 'dendyrezki@yahoo.com'),
(184, 'BD', '0', 'infiniti', 'JKT', '02125558785', 'dendyrezki@yahoo.com'),
(185, 'BD', 'Aplikasi Interactive Learning', 'infiniti', 'JKT', '02125558785', 'dendyrezki@yahoo.com'),
(186, 'BD', 'Aplikasi Interactive Learning', 'infiniti', 'JKT', '02125558785', 'dendyrezki@yahoo.com'),
(187, 'Infiniti BD', 'Aplikasi NextEdu', 'Infiniti', 'JKT', '021', 'd@u.com'),
(188, 'advdav', 'Aplikasi Biolearn', 'advdav', 'advdav', '021', 'lorem@yahoo.com'),
(189, 'Indocamp', 'Aplikasi Yuk Belajar', 'Smart School', 'Jl. Bangka Raya No.2', '0217192769', 'indocamp@yahoo.com'),
(190, 'Indocamp', 'Aplikasi Yuk Belajar', 'Smart School', 'Jl. Bangka Raya No. 2', '0217192769', 'indocamp@yahoo.com'),
(191, 'Indocamp', 'Aplikasi Yuk Belajar', 'Smart School', 'Jl. Bangka Raya No. 2', '0217192769', 'indocamp@yahoo.com'),
(192, 'Heribertus Eko', 'Aplikasi Yuk Belajar', 'SD Stella Maris', 'Jl. Artha Kencana Kav. C 1 No. 1, Kencana Loka, Sektor XII, Serpong', '7566766', 'penyooke@gmail.com'),
(193, 'Heribertus Eko', 'Aplikasi Yuk Belajar', 'SD Stella Maris', 'Jl. Artha Kencana Kav. C 1 No. 1, Kencana Loka, Sektor XII, Serpong', '7566766', 'penyooke@gmail.com'),
(194, 'Infiniti', 'Aplikasi Interactive Learning', 'Infiniti', 'Infiniti', '021', 'info.irs@co.id'),
(195, 'Infiniti', 'Aplikasi Interactive Learning', 'Infiniti', 'infiniti', '021', 'info.irs@co.id'),
(196, 'infiniti', 'Aplikasi Interactive Learning', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(197, 'infiniti', 'Aplikasi Interactive Learning', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(198, 'infiniti', 'Aplikasi Interactive Learning', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(199, 'infiniti', '0', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(200, 'infiniti', 'Aplikasi NextEdu', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(201, 'infiniti', 'Aplikasi NextEdu', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(202, 'infiniti', 'Aplikasi NextEdu', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(203, 'infiniti', 'Aplikasi NextEdu', 'infiniti', 'infiniti', '021', 'dendy.rezkisano@irs.co.id'),
(204, 'infiniti rs', '0', 'IRS', 'IRS', '021', 'dendy.rezkisano@irs.co.id'),
(205, 'infiniti', '0', 'IRS', 'IRS\r\n', '021', 'dendy.rezkisano@irs.co.id'),
(206, 'irs', 'Aplikasi NextEdu', 'irs', 'irs', '021', 'dendy.rezkisano@irs.co.id'),
(207, 'irs', 'Aplikasi NextEdu', 'IRS', 'IRS', '021', 'dendy.rezkisano@irs.co.id'),
(208, 'irs', 'Aplikasi NextEdu', 'irs', 'irs', '021', 'dendy.rezkisano@irs.co.id'),
(209, 'irs', 'Aplikasi NextEdu', 'irs', 'irs', '021', 'dendyrezki@yahoo.com'),
(210, 'IRS', 'Aplikasi NextEdu', 'IRS', 'IRS', '021', 'dendyrezki@yahoo.com'),
(211, 'risdo', 'Aplikasi NextEdu', 'Assesmatik', 'Kokas', '021', 'iyanrisdo@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activasi`
--
ALTER TABLE `activasi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activator_codes`
--
ALTER TABLE `activator_codes`
 ADD PRIMARY KEY (`id_pengguna`), ADD UNIQUE KEY `serial_no` (`serial_nomor`), ADD UNIQUE KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batas_pengguna`
--
ALTER TABLE `batas_pengguna`
 ADD KEY `fk_batasan_menu` (`kode_menu`);

--
-- Indexes for table `serial`
--
ALTER TABLE `serial`
 ADD PRIMARY KEY (`no`);

--
-- Indexes for table `serial_number`
--
ALTER TABLE `serial_number`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
 ADD PRIMARY KEY (`id_pengguna`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activasi`
--
ALTER TABLE `activasi`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `activator_codes`
--
ALTER TABLE `activator_codes`
MODIFY `id_pengguna` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `serial`
--
ALTER TABLE `serial`
MODIFY `no` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT for table `serial_number`
--
ALTER TABLE `serial_number`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_data`
--
ALTER TABLE `user_data`
MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=212;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
