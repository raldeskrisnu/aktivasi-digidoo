<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>404 Not Found</title>
    <!-- Link CSS -->
    <?php include 'assets/link_css.php';   ?>
</head>
<body background="assets/img/bg.jpg" align="center">

<h1 style="margin-top:250px;"> Sayang Sekali, Halaman Yang Anda Cari Tidak Tersedia </h1>
<a href="http://www.acg.mydigidoo.com/" class="btn btn-info" role="button">Kembali Ke Halaman Utama</a>
	
	<div class="col-md-12" style="margin-top:50px; margin-bottom:50px; text-align:center;">© Copyright 2016. PT.Infiniti Reka Solusi</div>
	<!-- link JS -->
    <?php include 'assets/link_js.php'; ?>
</body>
</html>