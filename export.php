<?php
require('assets/WriteHTML.php');

$pdf=new PDF_HTML();

$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true, 15);

$pdf->AddPage();
$pdf->Image('assets/img/logo.png',18,13,33);
$pdf->SetFont('Arial','B',16);
$pdf->WriteHTML('<para></para><br><br>Data Aktivasi Aplikasi');

$pdf->SetFont('Arial','B',10); 
$htmlTable='<TABLE>
<TR>
<TD>Nama Pengguna</TD>
<TD>'.$_POST['nama_pengguna'].'</TD>
</TR>
<TR>
<TD>Jenis Aplikasi</TD>
<TD>'.$_POST['jenis_aplikasi'].'</TD>
</TR>
<TR>
<TD>Asal Sekolah</TD>
<TD>'.$_POST['nama_sekolah'].'</TD>
</TR>
<TR>
<TD>Alamat</TD>
<TD>'.$_POST['alamat_sekolah'].'</TD>
</TR>
<TR>
<TD>Nomor Telepon</TD>
<TD>'.$_POST['nomor_telepon'].'</TD>
</TR>
<TR>
<TD>Email</TD>
<TD>'.$_POST['email'].'</TD>
</TR>
<TR>
<TD>Nomor Serial</TD>
<TD>'.$_POST['serial_nomor'].'</TD>
</TR>
<TR>
<TD>Nomor Registrasi</TD>
<TD>'.$_POST['registrasi_nomor'].'</TD>
</TR>
<TR>
<TD>Nomor Aktivasi</TD>
<TD>'.$_POST['aktivasi_nomor'].'</TD>
</TR>
</TABLE>';
$pdf->WriteHTML2("<br><br><br>$htmlTable");
$pdf->SetFont('Arial','B',6);
$pdf->Output(); 
?>