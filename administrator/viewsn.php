<?php
session_start();
        if(!isset($_SESSION['username']))
        {
        header("location:index.php");
        }
    ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>View Serial Number</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/sb-admin.css" rel="stylesheet">
    <link href="assets/css/plugins/morris.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#aktivasi"><i class="fa fa-fw fa-arrows-v"></i> Aktivasi </a>
                        <ul id="aktivasi" class="collapse">
                            <li>
                                <a href="viewsn.php?page=1">Semua Data Aktivasi</a>
                            </li>
                            <li>
                                <a href="tampilkan_data.php">Jenis Aplikasi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="searching.php"><i class="glyphicon glyphicon-search"></i>Pencarian</a>
                    </li>
                </ul>
            </div>>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Aktivasi <small>View Serial Sumber</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>

                    <div class="col-md-12">
<!-- Leader Board Start --> 
                    <?php
                        include_once "../assets/connect.php";
                        
                        $sql = mysql_query("SELECT COUNT(id_pengguna) as num FROM user_data") or die (mysql_error());
                        $total_pages = mysql_fetch_array($sql)['num'];
                        
                        $no =1;
                        $limit = 50;
                        $adjacents = 50;
                        $targetpage = "viewsn.php";
                        
                        $page = $_GET['page'];
                        if($page)
                        {
                            $start = ($page - 1) * $limit;
                            $no = 1 + (($page - 1) * $limit);
                            
                            if($page == 1)
                            {
                                //include_once "getData.php";
                            }
                        }
                        else
                        {
                            $start = 0;
                        }
                        
                        /* Setup page vars for display. */
                        if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
                        $prev = $page - 1;                          //previous page is page - 1
                        $next = $page + 1;                          //next page is page + 1
                        $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
                        $lpm1 = $lastpage - 1;
                        
                        $pagination = "";
                        if($lastpage > 1)
                        {   
                            $pagination .= "<div class=\"pagination\">";
                            //previous button
                            if ($page > 1) 
                                $pagination.= "<a href=\"$targetpage?page=$prev\"><< Sebelumnya</a>";
                            else
                                $pagination.= "<span class=\"disabled\"><< Sebelumnya</span>";  
                            
                            //pages 
                            if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
                            {   
                                for ($counter = 1; $counter <= $lastpage; $counter++)
                                {
                                    if ($counter == $page)
                                        $pagination.= "<span class=\"current\">$counter</span>";
                                    else
                                        $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";                 
                                }
                            }
                            elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
                            {
                                //close to beginning; only hide later pages
                                if($page < 1 + ($adjacents * 2))        
                                {
                                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                                    {
                                        if ($counter == $page)
                                            $pagination.= "<span class=\"current\">$counter</span>";
                                        else
                                            $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";                 
                                    }
                                    $pagination.= "...";
                                    $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
                                    $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";       
                                }
                                //in middle; hide some front and some back
                                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                                {
                                    $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
                                    $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
                                    $pagination.= "...";
                                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                                    {
                                        if ($counter == $page)
                                            $pagination.= "<span class=\"current\">$counter</span>";
                                        else
                                            $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";                 
                                    }
                                    $pagination.= "...";
                                    $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
                                    $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";       
                                }
                                //close to end; only hide early pages
                                else
                                {
                                    $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
                                    $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
                                    $pagination.= "...";
                                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                                    {
                                        if ($counter == $page)
                                            $pagination.= "<span class=\"current\">$counter</span>";
                                        else
                                            $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";                 
                                    }
                                }
                            }
                            
                            //next button
                            if ($page < $counter - 1) 
                                $pagination.= "<a href=\"$targetpage?page=$next\">Berikutnya >></a>";
                            else
                                $pagination.= "<span class=\"disabled\">Berikutnya >></span>";
                            $pagination.= "</div>\n";       
                        }
                    ?>
                        <table class="table table-hover">
                                <thead>
                                    <th style="background-color:cyan; text-align:center;">No</th>
                                    <th style="background-color:cyan; text-align:center;">Nama</th>
                                    <th style="background-color:cyan; text-align:center;">Nama Sekolah</th>
                                    <th style="background-color:cyan; text-align:center;">Nomor Serial</th>
                                    <th style="background-color:cyan; text-align:center;">Action</th>
                                </thead>

                        <?php

                        $sql="SELECT user_data.id_pengguna, user_data.nama_pengguna, user_data.nama_sekolah,user_data.jenis_aplikasi ,user_data.alamat_sekolah, user_data.nomor_telepon,user_data.email , activator_codes.id_pengguna, activator_codes.serial_nomor, activator_codes.registrasi_nomor, activator_codes.aktivasi_nomor FROM user_data INNER JOIN activator_codes ON user_data.id_pengguna = activator_codes.id_pengguna ORDER BY user_data.id_pengguna DESC LIMIT $start, $limit";
                        
                        if($result = @mysql_query($sql)){
                            if(mysql_num_rows($result) > 0){
                                while($row = mysql_fetch_array($result)){
                        ?>
                                <tbody align="center">
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $row['nama_pengguna'] ?></td>
                                    <td><?php echo $row['nama_sekolah'] ?></td>
                                    <td><?php echo $row['serial_nomor'] ?></td>
                                    <td><button class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $row['registrasi_nomor'] ?>">Lihat Data</button></div></td>
                                    <!-- Dialog Modal -->
                                    <div class="modal fade" id="<?php echo $row['registrasi_nomor'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel"><font face="Comic Sans MS">Data Aktivasi <?php echo $row['nama_pengguna'] ?> </font></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Start -->
                                                    <p>&nbsp;</p>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="nama_pengguna">Nama Pengguna</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="nama_pengguna" value="<?php echo $row['nama_pengguna'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="nama_sekolah">Asal Sekolah</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="nama_sekolah" value="<?php echo $row['nama_sekolah'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="jenis_aplikasi">Jenis Aplikasi</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="jenis_aplikasi" value="<?php echo $row['jenis_aplikasi'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="alamat_sekolah">Alamat</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="alamat_sekolah" value="<?php echo $row['alamat_sekolah'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="nomor_telepon">No.Telepon</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="nomor_telepon" value="<?php echo $row['nomor_telepon'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="email">Email</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="email" value="<?php echo $row['email'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="serial_nomor">Nomor Serial</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="serial_nomor" value="<?php echo $row['serial_nomor'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="registrasi_nomor">Nomor Registrasi</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="registrasi_nomor" value="<?php echo $row['registrasi_nomor'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div><p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="aktivasi_nomor">Nomor Aktivasi</label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="aktivasi_nomor" value="<?php echo $row['aktivasi_nomor'] ?>" class="form-control input-md" readonly >
                                                        </div>
                                                    </div>

                                                    <p>&nbsp;</p>
                                                    <!-- End -->                                                           
                                                </div>
                                                <div class="modal-footer">
                                                    <p> © Copyright 2015. PT. Infiniti Reka Solusi</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tbody>
                        <?php
                                }
                            }echo "</table>";
                        } else{
                            echo "ERROR: Could not able to execute $sql. ";
                        }
                    ?>
                    
                    <?php echo "<div align='center'>"?>
                    <?=$pagination?>
                    <?php echo "</div>"?>
 <!-- Leader Board End -->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins/morris/raphael.min.js"></script>
    <script src="assets/js/plugins/morris/morris.min.js"></script>
    <script src="assets/js/plugins/morris/morris-data.js"></script>

</body>

</html>
