<?php
session_start();
        if(!isset($_SESSION['username']))
        {
        header("location:index.php");
        }
include '../assets/connect.php';
    ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>View Serial Number</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/sb-admin.css" rel="stylesheet">
    <link href="assets/css/plugins/morris.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#aktivasi"><i class="fa fa-fw fa-arrows-v"></i> Aktivasi </a>
                        <ul id="aktivasi" class="collapse">
                            <li>
                                <a href="viewsn.php?page=1">Semua Data Aktivasi</a>
                            </li>
                            <li>
                                <a href="tampilkan_data.php">Jenis Aplikasi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="searching.php"><i class="glyphicon glyphicon-search"></i>Pencarian</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Aktivasi <small>View Serial Sumber</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="glyphicon glyphicon-search"></i> Tampilkan Data
                            </li>
                        </ol>
                    </div>
                <script src="assets/js/jquery.min.js"></script>    
                <script type="text/javascript">
                    $(document).ready(function() {
                        <!-- event textbox keyup
                        $("#txtcari").keyup(function() {
                            var strcari = $("#txtcari").val(); <!-- mendapatkan nilai dari textbox -->
                            if (strcari != "") <!-- jika value strcari tidak kosong-->
                            {
                                $("#hasil").html("<img src='loading.gif'/>") <!-- menampilkan animasi loading -->
                                <!-- request data ke cari.php lalu menampilkan ke <div id="hasil"></div> -->
                                $.ajax({
                                    type:"post",
                                    url:"cari.php",
                                    data:"q="+ strcari,
                                    success: function(data){
                                        $("#hasil").html(data);
                                    }
                                });
                            }
                        });
                    });
                </script>

                <div> 
                <input type="text" name="textcari" id="txtcari" class="form-control" placeholder="Cari Data" />

                </div>
                <div id="hasil"></div>

            

                        

                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins/morris/raphael.min.js"></script>
    <script src="assets/js/plugins/morris/morris.min.js"></script>
    <script src="assets/js/plugins/morris/morris-data.js"></script>

</body>

</html>
