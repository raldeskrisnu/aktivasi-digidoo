<?php
session_start();
        if(!isset($_SESSION['username']))
        {
        header("location:index.php");
        }
include '../assets/connect.php';
    ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>View Serial Number</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/sb-admin.css" rel="stylesheet">
    <link href="assets/css/plugins/morris.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#aktivasi"><i class="fa fa-fw fa-arrows-v"></i> Aktivasi </a>
                        <ul id="aktivasi" class="collapse">
                            <li>
                                <a href="viewsn.php?page=1">Semua Data Aktivasi</a>
                            </li>
                            <li>
                                <a href="tampilkan_data.php">Jenis Aplikasi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="searching.php"><i class="glyphicon glyphicon-search"></i>Pencarian</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Aktivasi <small>View Serial Sumber</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="glyphicon glyphicon-search"></i> Tampilkan Data
                            </li>
                        </ol>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">

                        <form method="POST" action="">  
                            <div class="col-md-6">
                                <select name="txtsearch" class="form-control"> 
                                    <option>Silahkan Pilih Jenis Aplikasi</option>
                                        <option></option>
                                    <option value="15101">Biolearn Kelas 10</option>
                                    <option value="15111">Biolearn Kelas 11</option>
                                    <option value="15121">Biolearn Kelas 12</option>
                                        <option></option> 
                                    <option value="13041">Ilearn Bahasa Indonesia Kelas 4</option>  
                                    <option value="13051">Ilearn Bahasa Indonesia Kelas 5</option>
                                    <option value="13061">Ilearn Bahasa Indonesia Kelas 6</option> 
                                    <option value="13071">Ilearn Bahasa Indonesia Kelas 7</option> 
                                    <option value="13081">Ilearn Bahasa Indonesia Kelas 8</option> 
                                    <option value="13091">Ilearn Bahasa Indonesia Kelas 9</option> 
                                        <option></option> 
                                    <option value="14041">Ilearn PKn Kelas 4</option>
                                    <option value="14051">Ilearn PKn Kelas 5</option>
                                    <option value="14061">Ilearn PKn Kelas 6</option>
                                    <option value="14071">Ilearn PKn Kelas 7</option>
                                    <option value="14081">Ilearn PKn Kelas 8</option>
                                    <option value="14091">Ilearn PKn Kelas 9</option>
                                        <option></option> 
                                    <option value="12071">NextEdu Ekonomi Kelas 7</option>
                                    <option value="12081">NextEdu Ekonomi Kelas 8</option>
                                    <option value="12091">NextEdu Ekonomi Kelas 9</option>
                                        <option></option> 
                                    <option value="11071">NextEdu Geografi Kelas 7</option>
                                    <option value="11081">NextEdu Geografi Kelas 8</option>
                                    <option value="11091">NextEdu Geografi Kelas 9</option>
                                        <option></option> 
                                    <option value="10041">NextEdu IPS Kelas 4</option>
                                    <option value="10051">NextEdu IPS Kelas 5</option>
                                    <option value="10061">NextEdu IPS Kelas 6</option>
                                        <option></option>
                                    <option value="15103">Yuk Belajar Biologi Kelas 10</option>
                                    <option value="15113">Yuk Belajar Biologi Kelas 11</option>
                                    <option value="15123">Yuk Belajar Biologi Kelas 12</option>
                                        <option></option> 
                                    <option value="13043">Yuk Belajar Bahasa Indonesia Kelas 4</option>  
                                    <option value="13053">Yuk Belajar Bahasa Indonesia Kelas 5</option>
                                    <option value="13063">Yuk Belajar Bahasa Indonesia Kelas 6</option> 
                                    <option value="13073">Yuk Belajar Bahasa Indonesia Kelas 7</option> 
                                    <option value="13083">Yuk Belajar Bahasa Indonesia Kelas 8</option> 
                                    <option value="13093">Yuk Belajar Bahasa Indonesia Kelas 9</option> 
                                        <option></option> 
                                    <option value="14043">Yuk Belajar PKn Kelas 4</option>
                                    <option value="14053">Yuk Belajar PKn Kelas 5</option>
                                    <option value="14063">Yuk Belajar PKn Kelas 6</option>
                                    <option value="14073">Yuk Belajar PKn Kelas 7</option>
                                    <option value="14083">Yuk Belajar PKn Kelas 8</option>
                                    <option value="14093">Yuk Belajar PKn Kelas 9</option>
                                        <option></option> 
                                    <option value="12043">Yuk Belajar Ekonomi Kelas 7</option>
                                    <option value="12053">Yuk Belajar Ekonomi Kelas 8</option>
                                    <option value="12063">Yuk Belajar Ekonomi Kelas 9</option>
                                        <option></option> 
                                    <option value="11073">Yuk Belajar Geografi Kelas 7</option>
                                    <option value="11083">Yuk Belajar Geografi Kelas 8</option>
                                    <option value="11093">Yuk Belajar Geografi Kelas 9</option>
                                        <option></option> 
                                    <option value="10043">Yuk Belajar IPS Kelas 4</option>
                                    <option value="10053">Yuk Belajar IPS Kelas 5</option>
                                    <option value="10063">Yuk Belajar IPS Kelas 6</option>
                                </select>    
                            </div>

                            <div class="col-md-6">
                                <input type="hidden" name="kategoriaplikasi" value="serial_nomor">
                                <input type="submit" value="Tampilkan" name="submit" class="btn btn-info" role="button" /> 
                            </div>
                        </div><p>&nbsp;</p>


                        <table class="table table-hover">
                            <thead>
                                <th style="background-color:cyan; text-align:center;">No</th>
                                <th style="background-color:cyan; text-align:center;">Nama Pengguna</th>
                                <th style="background-color:cyan; text-align:center;">Nama Sekolah</th>
                                <th style="background-color:cyan; text-align:center;">Nomor Serial</th>
                                <th style="background-color:cyan; text-align:center;">Nomor Registrasi</th>
                                <th style="background-color:cyan; text-align:center;">Nomor Aktivasi</th>

                            </thead>
                            <?php  
                                if (isset($_POST['submit'])) {  
                                $search = $_POST['txtsearch'];  
                                $kategoriaplikasi = $_POST['kategoriaplikasi'];  
                                 
                                //$sql = "SELECT * FROM activator_codes WHERE $kategoriaplikasi LIKE '%$search%'";
                                $sql="SELECT user_data.id_pengguna, user_data.nama_pengguna, user_data.nama_sekolah, activator_codes.id_pengguna, activator_codes.serial_nomor, activator_codes.registrasi_nomor, activator_codes.aktivasi_nomor FROM user_data INNER JOIN activator_codes ON user_data.id_pengguna = activator_codes.id_pengguna WHERE $kategoriaplikasi LIKE '%$search%'";  
                                $result = mysql_query($sql) or die('Error, list activator_codes failed. ' . mysql_error());  
                                $no = 1;  
                                if (mysql_num_rows($result) == 0) {  
                                echo '<div class="alert alert-danger" role="alert">
                                        <a class="alert-link">Pencarian Tidak Ditemukan</a>
                                        </div>';  
                                } else {  
                                echo '<p></p>';  
                            
                                while ($row = mysql_fetch_array($result)) {  
                                extract($row);  

                            ?>

                                <tbody>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $nama_pengguna ?></td>
                                    <td><?php echo $nama_sekolah ?></td>
                                    <td><?php echo $serial_nomor ?></td>
                                    <td><?php echo $registrasi_nomor ?></td>
                                    <td><?php echo $aktivasi_nomor ?></td>
                                </tbody>
    
                             <?php  
                                } echo "</table>";  
                               }  
                              }  
                            ?>
                        </form> 
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins/morris/raphael.min.js"></script>
    <script src="assets/js/plugins/morris/morris.min.js"></script>
    <script src="assets/js/plugins/morris/morris-data.js"></script>

</body>

</html>
