<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Panduan Aktivasi</title>
    <!-- Link CSS -->
    <?php include 'assets/link_css.php';   ?>
</head>
<body background="assets/img/bg.jpg" style="background-position: center; background-size:100%; background-repeat: no-repeat; background-attachment: fixed;">

            <h1 style="text-align: center;">Panduan Aktivasi Melalui Website</h1>
            <p style="text-align: center;">&nbsp;</p>
            <h5 style="text-align: center;">Berikut adalah tampilan halaman utama website <a href="http://acg.mydigidoo.com">www.acg.mydigidoo.com</a>,Untuk melakukan aktivasi, silahkan klik Icon Kunci, selanjutnya&nbsp;anda akan di arahkan ke halaman Aktivasi</h5>
            <p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="assets/img/1.PNG" alt="" width="75%" height="25%" /></p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <h5 style="text-align: center;">Isilah kolom yang tersedia sesuai dengan data anda, setelah semua kolom terisi, lanjutkan dengan mengKlik tombol aktivasi</h5>
            <p style="text-align: center;"><img src="assets/img/3.PNG" alt="" width="75%" height="25%" /></p>
            <p style="text-align: center;">&nbsp;</p>
            <h5 style="text-align: center;">&nbsp;</h5>
            <h5 style="text-align: center;">Selamat Aktivasi Suksess, Copy-Paste Nomor Aktivasi anda kedalam Aplikasi yang anda Gunakan. Atau Download hasil aktivasi anda dengan menekan icon "Download"</h5>
            <h5 style="text-align: center;">Jika ada pertanyaan silahkan hubungi operator&nbsp;melalui layanan Chatt online kami dengan mengklik chatt online yang tersedia di pojok kanan bawah</h5>
            <p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="assets/img/4.PNG" alt="" width="75%" height="25%" /></p>

            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4" align="center">
                <p>&nbsp;</p><p>&nbsp;</p>
                    <a href="http://www.acg.mydigidoo.com/"><img src="assets/img/home.png" width="10%" title="Halaman Utama" style="margin-right:25px"></a>
                    <a href="activation"><img src="assets/img/aktivasi.png" width="10%" title="Aktivasi" style="margin-right:25px;margin-left:25px"></a><p>&nbsp;</p>
                </div>
            </div>


	<?php include 'assets/footer.php'; ?>
    <!-- link JS -->
    <?php include 'assets/link_js.php'; ?>
</body>
</html>