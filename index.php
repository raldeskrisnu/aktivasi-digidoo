<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Halaman Aktivasi Aplikasi</title>
    <?php include 'assets/link_css.php';   ?>
	<?php include 'assets/link_js.php'; ?>
</head>
<body background="assets/img/bg.jpg" style="background-position: center; background-size:100%; background-repeat: no-repeat; background-attachment: fixed;">
<center>
<img src="assets/img/welcom.png" style="margin-top:100px">

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12"><h2> Selamat Datang Di Halaman Aktivasi Aplikasi Dgidoo</h2></div>
		
			<div class="col-md-2"></div>

				<div class="col-md-8">
		  			<div class="panel-body">
		  			<!-- Content Start -->
						<div class="col-md-6">
									<h3> Klik Icon Untuk Aktivasi </h3>
								<a href="activation">
									<img src="assets/img/aktivasi.png" width="100px" title="Aktivasi">
						  		</a>
						  </div>

						<!-- Content Start -->
						<div class="col-md-6">
									<h3>Klik Icon Untuk Panduan Aktivasi</h3>
								<a href="userguide">
									<img src="assets/img/help.png" width="100px" title="Panduan Aktivasi">
						  		</a>
						</div>
						<!-- Content End -->

					</div>
				</div>

	<div class="col-md-2"></div>


		</div>
	</div>

</center>
	
	<?php include 'assets/footer.php'; ?>
</body>
</html>
