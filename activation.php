<?php error_reporting(E_ALL ^ E_DEPRECATED); ?> 
<html>
<head>
<meta charset="utf-8">
<title>Halaman Aktivasi</title>
<?php include 'assets/link_css.php'; ?>

<?php include 'assets/link_js.php'; ?>
</head>

<body background="assets/img/bg.jpg" style="background-position: center; background-size:100%; background-repeat: no-repeat; background-attachment: fixed;">
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-8" style="background-color:#fff; border:2px solid black; border-radius:20px; margin-top:50px; text-align:center;">
                    <p>&nbsp;</p><h2>Halaman Aktivasi</h2><p>&nbsp;</p>
                    <a href="http://www.acg.mydigidoo.com/"><img src="assets/img/home.png" width="50px" title="Halaman Utama" style="margin-right:25px"></a>
                    <a href="userguide"><img src="assets/img/help.png" width="50px" title="Panduan Aktivasi" style="margin-left:25px"></a><p>&nbsp;</p>
                <form action="success" class="form-horizontal" method="post" id="register_form">
                        <div class="form-group" style="margin-top:10px;">
                            <label class="col-md-3 control-label" for="name_pengguna">Nama Pengguna</label>  
                            <div class="col-md-9">
                            <input id="name_pengguna" name="name_pengguna" type="text" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="jenis_aplikasi">Jenis Aplikasi</label>
                            <div class="col-md-9">
                                <select id="jenis_aplikasi" name="jenis_aplikasi" class="form-control">
                                    <option value="0">Pilih Jenis Aplikasi</option>
									<option value="Aplikasi Digidoo SD">Aplikasi Digidoo SD</option>
                                    <option value="Aplikasi Digidoo SMP">Aplikasi Digidoo SMP</option>
                                    <option value="Aplikasi Digidoo SMA">Aplikasi Digidoo SMA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name_text">Nama Sekolah</label>  
                            <div class="col-md-9">
                                <input id="name_text" name="name_text" type="text"  class="form-control input-md" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="alamat_text">Alamat</label>
                            <div class="col-md-9">                     
                                <textarea class="form-control" id="alamat_text" name="alamat_text" row="5" required></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="telp_text">Nomor Telepon</label>  
                            <div class="col-md-9">
                                <input id="telp_text" name="telp_text" type="text" onkeypress='validate(event)' class="form-control input-md" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email_text">Email</label>  
                            <div class="col-md-9">
                                <input id="mail" name="email_text" type="email"  class="form-control input-md" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="serial_text">Nomor Serial</label>  
                            <div class="col-md-9">
                                <input id="serial_text" name="serial_text" type="text" class="form-control input-md" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="registrasi_text">Nomor Registrasi</label>  
                            <div class="col-md-9">
                                <input id="registrasi_text" name="registrasi_text" type="text" class="form-control input-md" required> 
                            </div>
                        </div>

                        <input id="registrasi_text2" name="registrasi_text2" type="hidden"  class="form-control input-md" readonly>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="singlebutton"></label>
                            <div class="col-md-9" align="right">
                                <input type="submit" onclick="activation()" id="submit" name="submit"  class="btn btn-primary" role="button" value="Aktivasi">
                            </div>
                        </div><p>&nbsp;</p>
                </form>
            </div>
        <div class="col-md-2"></div>
    </div><!-- Row --><?php include 'assets/footer.php'; ?>
</div><!-- Container -->     

</body>
</html>
