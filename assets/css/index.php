<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>404 Not Found</title>
    <!-- Link CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap.css.map" rel="stylesheet">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-theme.css.map" rel="stylesheet">
    <link href="../assets/css/bootstrap-theme.min.css" rel="stylesheet">

</head>
<body background="../assets/img/bg.jpg" align="center">

<h1 style="margin-top:250px;"> Sayang Sekali, Halaman Yang Anda Cari Tidak Tersedia </h1>
<a href="http://www.acg.infisupport.com/" class="btn btn-info" role="button">Kembali Ke Halaman Utama</a>
	
	<div class="col-md-12" style="margin-top:50px; margin-bottom:50px; text-align:center;">© Copyright 2015. PT.Infiniti Reka Solusi</div>
	<!-- link JS -->
 
<script type="text/javascript" src="../assets/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.js"></script>
<script type="text/javascript" src="../assets/js/npm.js"></script>
<script type="text/javascript">
var $_Tawk_API={},$_Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/55767649e4de91441f664cc5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

</body>
</html>